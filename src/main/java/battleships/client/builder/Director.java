package battleships.client.builder;

import battleships.client.models.ships.Ship;

public class Director {
    private IBuilder builder;

    public Director() {
    }

    public Ship getReady() {
        this.builder = new SBuilder();
        return this.builder.startNew().addAttack().addShipStructure().buildShip();
    }

}

