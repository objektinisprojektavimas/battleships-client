package battleships.client.builder;

import battleships.client.models.ships.Ship;

public class SBuilder implements IBuilder {
    Ship e = null;
    static int qty = 1;

    public SBuilder() {
    }

    public IBuilder addAttack() {
        System.out.println("Builder: add attack");
        return this;
    }

    public IBuilder addShipStructure() {
        System.out.println("Builder: add Ship structure");
        return this;
    }


    public IBuilder startNew() {
        this.e = new Ship();
        return this;

    }

    public Ship buildShip() {
        return this.e;
    }
}