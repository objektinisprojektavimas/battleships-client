package battleships.client.builder;

public class Prototype implements Cloneable {
    public Prototype() {
    }

    public Prototype clone() {
        try {
            return (Prototype)super.clone();
        } catch (CloneNotSupportedException var2) {
            var2.printStackTrace();
            return null;
        }
    }
}