package battleships.client.builder;

import battleships.client.models.ships.Ship;

public interface IBuilder {
    IBuilder addAttack();

    IBuilder addShipStructure();

    IBuilder startNew();

    Ship buildShip();
}