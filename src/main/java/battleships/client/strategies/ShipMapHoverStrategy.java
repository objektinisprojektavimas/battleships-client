package battleships.client.strategies;

import battleships.client.constants.StyleConstants;
import battleships.client.enums.Orientation;
import battleships.client.enums.ShipType;
import battleships.client.factory.ShipFactory;
import battleships.client.models.ships.Ship;
import battleships.client.ui.components.common.GameMap;
import battleships.client.ui.components.common.MapCell;

import java.util.ArrayList;
import java.util.List;

public class ShipMapHoverStrategy extends AbstractMapHoverStrategy {

    private Ship ship;

    public ShipMapHoverStrategy(GameMap gameMap, ShipType shipType) {
        super(gameMap);
        this.ship = ShipFactory.createShip(shipType);
    }

    @Override
    public void hover(int row, int column) {
        List<MapCell> mapCells = getMapCells(row, column);
        if (isValidPosition(row, column)) {
            mapCells.forEach(mapCell -> mapCell.setColor("#A2BA5C"));
        } else {
            mapCells.forEach(mapCell -> mapCell.setColor("#E57373"));
        }
    }

    @Override
    public void unHover(int row, int column) {
        List<MapCell> mapCells = getMapCells(row, column);
        mapCells.forEach(mapCell -> mapCell.setColor(StyleConstants.DEFAULT_MAP_CELL_COLOR));
    }

    private boolean isValidPosition(int row, int column) {
        if (ship.getOrientation() == Orientation.HORIZONTAL) {
            for (int i = column; i < column + ship.getLenght(); i++) {
                if (!gameMap.getMapCell(row, i).isPresent()) {
                    return false;
                }
            }
        } else if (ship.getOrientation() == Orientation.VERTICAL) {
            for (int i = row; i > row - ship.getLenght(); i--) {
                if (!gameMap.getMapCell(i, column).isPresent()) {
                    return false;
                }
            }
        }
        return true;
    }

    private List<MapCell> getMapCells(int row, int column) {
        List<MapCell> mapCells = new ArrayList<>();
        if (ship.getOrientation() == Orientation.HORIZONTAL) {
            for (int i = column; i < column + ship.getLenght(); i++) {
                gameMap.getMapCell(row, i).ifPresent(mapCells::add);
            }
        } else if (ship.getOrientation() == Orientation.VERTICAL) {
            for (int i = row; i > row - ship.getLenght(); i--) {
                gameMap.getMapCell(i, column).ifPresent(mapCells::add);
            }
        }
        return mapCells;
    }
}
