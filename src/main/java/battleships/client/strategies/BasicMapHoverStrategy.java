package battleships.client.strategies;

import battleships.client.constants.StyleConstants;
import battleships.client.ui.components.common.GameMap;

public class BasicMapHoverStrategy extends AbstractMapHoverStrategy {

    public BasicMapHoverStrategy(GameMap gameMap) {
        super(gameMap);
    }

    @Override
    public void hover(int row, int column) {
        gameMap.getMapCell(row, column).ifPresent(node -> node.setColor("#DDDDDD"));
    }

    @Override
    public void unHover(int row, int column) {
        gameMap.getMapCell(row, column).ifPresent(node -> node.setColor(StyleConstants.DEFAULT_MAP_CELL_COLOR));
    }
}
