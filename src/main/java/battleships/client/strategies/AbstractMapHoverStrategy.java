package battleships.client.strategies;

import battleships.client.models.ships.Ship;
import battleships.client.ui.components.common.GameMap;

public abstract class AbstractMapHoverStrategy {

    protected GameMap gameMap;

    public AbstractMapHoverStrategy(GameMap gameMap) {
        this.gameMap = gameMap;
    }

    public abstract void hover(int row, int column);

    public abstract void unHover(int row, int column);
}
