package battleships.client.factory;

import battleships.client.enums.ShipType;
import battleships.client.strategies.AbstractMapHoverStrategy;
import battleships.client.strategies.BasicMapHoverStrategy;
import battleships.client.strategies.ShipMapHoverStrategy;
import battleships.client.ui.components.common.GameMap;

public class MapHoverStrategyFactory {

    public static AbstractMapHoverStrategy createMapHoverStrategy(ShipType shipType, GameMap gameMap) {
        switch (shipType){
            case BATTLESHIP:
                return new ShipMapHoverStrategy(gameMap, ShipType.BATTLESHIP);
            case CARRIER:
                return new ShipMapHoverStrategy(gameMap, ShipType.CARRIER);
            case DESTROYER:
                return new ShipMapHoverStrategy(gameMap, ShipType.DESTROYER);
            case SUBMARINE:
                return new ShipMapHoverStrategy(gameMap, ShipType.SUBMARINE);
            case CRUISER:
                return new ShipMapHoverStrategy(gameMap, ShipType.BATTLESHIP);
            default:
                return new BasicMapHoverStrategy(gameMap);
        }
    }
}
