package battleships.client.factory;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public class UnitFactory {

    public UnitFactory createFactory(String factoryType){
        switch (factoryType) {
            case "ship":
                System.out.println("Ship shipFactory has been created.");
                return new ShipFactory();
            default:
                System.out.println("Factory was not created (Unknown type)");
                throw new NotImplementedException();
        }
    }

}
