package battleships.client.factory;

import battleships.client.models.ships.Battleship;
import battleships.client.models.ships.Carrier;
import battleships.client.models.ships.Cruiser;
import battleships.client.models.ships.Destroyer;
import battleships.client.models.ships.Ship;
import battleships.client.models.ships.Submarine;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;
import battleships.client.factory.UnitFactory;
import battleships.client.enums.ShipType;

public class ShipFactory extends UnitFactory {

    public static Ship createShip(ShipType type){
        switch (type){
            case CARRIER:
                System.out.println("Carrier ship type has been created.");
                return new Carrier();
            case DESTROYER:
                System.out.println("Destroyer ship type has been created.");
                return new Destroyer();
            case SUBMARINE:
                System.out.println("Submarine ship type has been created.");
                return new Submarine();
            case CRUISER:
                System.out.println("Cruiser ship type has been created.");
                return new Cruiser();
            case BATTLESHIP:
                System.out.println("Battleship ship ship type has been created.");
                return new Battleship();
            default:
                System.out.println("Ship was not created (Unknown ship type)");
                throw new NotImplementedException();
        }
    }
}
