package battleships.client.enums;

public enum ShipType {
    CARRIER("Carrier"),
    DESTROYER("Destroyer"),
    SUBMARINE("Submarine"),
    CRUISER("Cruiser"),
    BATTLESHIP("Battleship");

    private String text;

    ShipType(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }
}
