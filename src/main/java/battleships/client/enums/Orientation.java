package battleships.client.enums;

public enum Orientation {
    HORIZONTAL,
    VERTICAL
}
