package battleships.client.ui.components.common;

import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import org.omg.CORBA.PUBLIC_MEMBER;

public class ComponentUtils {

    public static Separator getVerticalSeparator() {
        final Separator separator = new Separator();
        separator.setOrientation(Orientation.VERTICAL);
        return separator;
    }

    public static Separator getHorizontalSeparator() {
        final Separator separator = new Separator();
        separator.setOrientation(Orientation.HORIZONTAL);
        return separator;
    }

    public static HBox getEmptyRow(Insets insets) {
        final HBox row = new HBox();
        row.setPadding(insets);
        return row;
    }

    public static HBox getEmptyRow() {
        final HBox row = new HBox();
        row.setPadding(new Insets(10, 0, 10, 0));
        return row;
    }

    public static Label getDefaultLabel(String text) {
        Label label = new Label(text);
        label.setFont(new Font("Arial", 20));
        return label;
    }

    public static Label getLabel(String text, CssStyleHelper cssStyleHelper) {
        Label label = new Label(text);
        String s = cssStyleHelper.getStyle();
        label.setStyle(cssStyleHelper.getStyle());
        return label;
    }

    public static Button getButton(String text) {
        Button button = new Button(text);
        return button;
    }
}
