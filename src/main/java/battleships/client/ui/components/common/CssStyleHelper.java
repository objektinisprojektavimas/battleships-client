package battleships.client.ui.components.common;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class CssStyleHelper {

    private Map<String, String> style = new HashMap<>();

    public CssStyleHelper() {}

    public CssStyleHelper padding(int padding) {
        style.put("padding", "-fx-padding: " + padding + "px;");
        return this;
    }

    public CssStyleHelper paddingTop(int padding) {
        style.put("paddingTop", "-fx-padding-top: " + padding + "px;");
        return this;
    }

    public CssStyleHelper paddingRight(int padding) {
        style.put("paddingRight", "-fx-padding-right: " + padding + "px;");
        return this;
    }

    public CssStyleHelper paddingBottom(int padding) {
        style.put("paddingBottom", "-fx-padding-bottom: " + padding + "px;");
        return this;
    }

    public CssStyleHelper paddingLeft(int padding) {
        style.put("paddingLeft", "-fx-padding-left: " + padding + "px;");
        return this;
    }

    public CssStyleHelper borderInsets(int borderInsets) {
        style.put("borderInsets", "-fx-border-insets: " + borderInsets + "px;");
        return this;
    }

    public CssStyleHelper borderColor(String borderColor) {
        style.put("borderColor", "-fx-border-color: " + borderColor + ";");
        return this;
    }

    public CssStyleHelper borderWidth(int width) {
        style.put("borderWidth", "-fx-border-width: " + width + ";");
        return this;
    }

    public CssStyleHelper backgroundColor(String backgroundColor) {
        style.put("backgroundColor", "-fx-background-color: " + backgroundColor + ";");
        return this;
    }

    public CssStyleHelper fontFamily(String fontFamily) {
        style.put("fontFamily", "-fx-font-family: " + fontFamily + ";");
        return this;
    }

    public CssStyleHelper fontSize(int fontSize) {
        style.put("fontSize", "-fx-font-size: " + fontSize + "px;");
        return this;
    }

    // [ normal | italic | oblique ]
    public CssStyleHelper fontStyle(String fontStyle) {
        style.put("fontStyle", "-fx-font-style: " + fontStyle + ";");
        return this;
    }

    // [ normal | bold | bolder | lighter | 100 | 200 | 300 | 400 | 500 | 600 | 700 | 800 | 900 ]
    public CssStyleHelper fontWeight(String fontWeight) {
        style.put("fontWeight", "-fx-font-weight: " + fontWeight + ";");
        return this;
    }

    // center | left | right
    public CssStyleHelper alignment(String alignment) {
        style.put("alignment", "-fx-alignment: " + alignment + ";");
        return this;
    }

    public CssStyleHelper borderRadius(int borderRadius) {
        style.put("borderRadius", "-fx-border-radius: " + borderRadius + ";");
        return this;
    }

    public CssStyleHelper backgroundRadius(int backgroundRadius) {
        style.put("backgroundRadius", "-fx-background-radius: " + backgroundRadius + "px;");
        return this;
    }

    public String getStyle() {
        return style.entrySet()
                    .stream()
                    .map(Map.Entry::getValue)
                    .collect(Collectors.joining());
    }
}
