package battleships.client.ui.components.common;

import battleships.client.constants.StyleConstants;
import javafx.scene.layout.StackPane;

public class MapCell extends StackPane {

    private GameMap gameMap;
    private CssStyleHelper cssStyleHelper = new CssStyleHelper();

    public MapCell(GameMap gameMap){
        this.gameMap = gameMap;
        setStyleProperties();
        setOnMouseEnteredEvent();
        setOnMouseExitedEvent();
    }

    private void setStyleProperties() {
        setStyle(cssStyleHelper.backgroundColor(StyleConstants.DEFAULT_MAP_CELL_COLOR).getStyle());
    }

    public CssStyleHelper setColor(String color) {
        cssStyleHelper.backgroundColor(color);
        updateStyle();
        return cssStyleHelper;
    }

    private void setOnMouseEnteredEvent() {
        setOnMouseEntered(event -> gameMap.notifyOnMouseEntered(this));
    }

    private void setOnMouseExitedEvent() {
        setOnMouseExited(event -> gameMap.notifyOnMouseExited(this));
    }

    public void updateStyle() {
        setStyle(cssStyleHelper.getStyle());
    }
}
