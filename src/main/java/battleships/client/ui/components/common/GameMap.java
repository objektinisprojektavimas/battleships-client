package battleships.client.ui.components.common;

import battleships.client.strategies.AbstractMapHoverStrategy;
import battleships.client.strategies.BasicMapHoverStrategy;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;

import java.util.Optional;
import java.util.stream.IntStream;

public class GameMap extends GridPane {

    private int rows;
    private int columns;

    private AbstractMapHoverStrategy hoverStrategy;

    public GameMap(int rows, int columns) {
        this.rows = rows;
        this.columns = columns;
        this.hoverStrategy = new BasicMapHoverStrategy(this);
        createMapCells();
        setConstraints();
        setStyleProperties();
    }

    private void createMapCells() {
        for (int i = 0; i < rows; i++) {
            for (int k = 0; k < columns; k++) {
                add(new MapCell(this), k, i);
            }
        }
    }

    private void setConstraints() {
        IntStream.range(0, rows).forEach(row -> getRowConstraints().add(getRowConstraint()));
        IntStream.range(0, columns).forEach(column -> getColumnConstraints().addAll(getColumnConstraint()));
    }

    public Optional<MapCell> getMapCell(int row, int column) {
        try {
        return getChildren().stream()
                .filter(node -> GridPane.getRowIndex(node) == row && GridPane.getColumnIndex(node) == column)
                .map(node -> (MapCell)node)
                .findFirst();
        } catch (NullPointerException ex) {
            return Optional.empty();
        }
    }

    public AbstractMapHoverStrategy getHoverStrategy() {
        return hoverStrategy;
    }

    public void setHoverStrategy(AbstractMapHoverStrategy hoverStrategy) {
        this.hoverStrategy = hoverStrategy;
    }

    public void notifyOnMouseEntered(MapCell mapCell) {
        int row = getRowIndex(mapCell);
        int column = getColumnIndex(mapCell);
        hoverStrategy.hover(row, column);
    }

    public void notifyOnMouseExited(MapCell mapCell) {
        int row = getRowIndex(mapCell);
        int column = getColumnIndex(mapCell);
        hoverStrategy.unHover(row, column);
    }

    private ColumnConstraints getColumnConstraint() {
        ColumnConstraints columnConstraints = new ColumnConstraints();
        columnConstraints.setPercentWidth(100 / columns);
        return columnConstraints;
    }

    private RowConstraints getRowConstraint() {
        RowConstraints rowConstraints = new RowConstraints();
        rowConstraints.setPercentHeight(100 / rows);
        return rowConstraints;
    }

    public void setSize(int width, int height) {
        setPrefSize(width, height);
    }

    private void setStyleProperties() {
        setMinSize(400, 400);
        setPrefSize(400, 400);

        setVgap(0);
        setHgap(0);
        setGridLinesVisible(true);

        setStyle(new CssStyleHelper().padding(25).getStyle());
    }
}
