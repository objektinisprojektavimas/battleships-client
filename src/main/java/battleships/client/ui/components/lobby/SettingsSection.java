package battleships.client.ui.components.lobby;

import battleships.client.constants.GameConstants;
import battleships.client.models.GameSettings;
import battleships.client.services.ConnectionService;
import battleships.client.ui.components.common.ComponentUtils;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Duration;

public class SettingsSection extends VBox {

    private ToggleGroup toggleGroup;
    private RadioButton fixedNumberRB;
    private RadioButton unsunkenShipsRB;
    private RadioButton largestShipRB;

    private Label gameStartStateLabel;

    private TextField rows;
    private TextField columns;

    private Timeline settingsUpdateLoop;

    private boolean settingsDisabled = false;

    private ConnectionService connectionService = new ConnectionService();

    public SettingsSection() {
        createToggleGroups();
        createRadioButtons();
        createTextsFields();
        createLabels();
        createSettingsUpdateLoop();
        initialize();
        setEvents();
        startSettingsUpdateLoop();
    }

    private void createToggleGroups() {
        this.toggleGroup = new ToggleGroup();
    }

    private void createRadioButtons() {
        this.fixedNumberRB = createRadioButton("Fixed number:   ", toggleGroup);
        this.unsunkenShipsRB = createRadioButton("Number of shots by number of unsunken ships", toggleGroup);
        this.largestShipRB = createRadioButton("Number of shots by largest unsunken ship", toggleGroup);

        this.fixedNumberRB.setSelected(true);
    }

    private void createTextsFields() {
        int width = 50;

        this.rows = new TextField("10");
        this.columns = new TextField("10");

        this.rows.setPrefWidth(width);
        this.columns.setPrefWidth(width);
    }

    private void createLabels() {
        this.gameStartStateLabel = new Label();
    }

    private void createSettingsUpdateLoop() {
        this.settingsUpdateLoop = new Timeline(
                new KeyFrame(Duration.millis(250), e -> {
                    GameSettings gameSettings = connectionService.getGameSettings();

                    if (!settingsAreEqual(gameSettings)) {
                        toggleSettingsSection(false);

                        updateSettingsUI(gameSettings);
                    }
                })
        );
        settingsUpdateLoop.setCycleCount(Animation.INDEFINITE);
    }

    public void startSettingsUpdateLoop() {
        this.settingsUpdateLoop.play();
    }

    public void stopSettingsUpdateLoop() {
        this.settingsUpdateLoop.stop();
    }

    public void toggleSettingsSection(boolean flag) {
        this.settingsDisabled = flag;
        fixedNumberRB.setDisable(flag);
        unsunkenShipsRB.setDisable(flag);
        largestShipRB.setDisable(flag);
        rows.setDisable(flag);
        columns.setDisable(flag);
    }

    private void initialize() {
        setPadding(new Insets(10, 0, 0, 10));

        getChildren().add(ComponentUtils.getEmptyRow());
        getChildren().addAll(new HBox(
                new Label("Game dimensions (rows x columns):    "),
                rows,
                new Label("  x  "),
                columns));

        getChildren().add(ComponentUtils.getEmptyRow());
        VBox numberOfShotsVBox = new VBox();
        numberOfShotsVBox.setPadding(new Insets(0, 0, 0, 25));
        numberOfShotsVBox.getChildren().addAll(
                fixedNumberRB,
                unsunkenShipsRB,
                largestShipRB);
        getChildren().add(new VBox(new Label("Number of shots per turn:"), numberOfShotsVBox));

        getChildren().add(ComponentUtils.getEmptyRow());
        HBox labelHBox = new HBox();
        labelHBox.setAlignment(Pos.CENTER);
        labelHBox.getChildren().add(gameStartStateLabel);
        getChildren().add(labelHBox);

        getChildren().add(ComponentUtils.getEmptyRow());
        getChildren().add(ComponentUtils.getEmptyRow());
    }

    private RadioButton createRadioButton(String text, ToggleGroup toggleGroup) {
        RadioButton radioButton = new RadioButton(text);
        radioButton.setToggleGroup(toggleGroup);
        return radioButton;
    }

    public boolean settingsAreEqual(GameSettings settings) {
        if (fixedNumberRB.isSelected() && !settings.getNumberOfShotsPerTurn().equals("FIXED_NUMBER_OF_SHOTS")) {
            return false;
        } else if (unsunkenShipsRB.isSelected() && !settings.getNumberOfShotsPerTurn().equals("NUMBER_OF_SHOTS_BY_UNSUNKEN_SHIPS")) {
            return false;
        } else if (largestShipRB.isSelected() && !settings.getNumberOfShotsPerTurn().equals("NUMBER_OF_SHOTS_BY_LARGEST_SHIP")) {
            return false;
        }

        return settings.getRows().equals(rows.getText()) && settings.getColumns().equals(columns.getText());
    }

    public GameSettings getGameSettings() {
        return new GameSettings(this);
    }

    public boolean isValid() {
        return GameSettings.isValid(this);
    }

    public boolean isSettingsDisabled() {
        return this.settingsDisabled;
    }

    public void updateSettingsUI(GameSettings settings) {
        this.rows.setText(settings.getRows());
        this.columns.setText(settings.getColumns());
        if (settings.getNumberOfShotsPerTurn().equals("FIXED_NUMBER_OF_SHOTS")) {
            this.fixedNumberRB.setSelected(true);
        } else if (settings.getNumberOfShotsPerTurn().equals("NUMBER_OF_SHOTS_BY_UNSUNKEN_SHIPS")) {
            this.unsunkenShipsRB.setSelected(true);
        } else if (settings.getNumberOfShotsPerTurn().equals("NUMBER_OF_SHOTS_BY_LARGEST_SHIP")) {
            this.largestShipRB.setSelected(true);
        }
    }

    private void setEvents() {
        this.fixedNumberRB.setOnAction(e -> {
            if (fixedNumberRB.isSelected()) {
                stopSettingsUpdateLoop();
            }
        });

        this.unsunkenShipsRB.setOnAction(e -> {
            if (unsunkenShipsRB.isSelected()) {
                stopSettingsUpdateLoop();
            }
        });

        this.largestShipRB.setOnAction(e -> {
            stopSettingsUpdateLoop();
        });

        this.rows.setOnKeyPressed(e -> {
            stopSettingsUpdateLoop();
        });

        this.columns.setOnKeyPressed(e -> {
            stopSettingsUpdateLoop();
        });
    }

    public String getRBSelectedText() {
        String text = null;
        if (unsunkenShipsRB.isSelected()) {
            text = GameConstants.NUMBER_OF_SHOTS_BY_UNSUNKEN_SHIPS;
        } else if (largestShipRB.isSelected()) {
            text = GameConstants.NUMBER_OF_SHOTS_BY_LARGEST_SHIP;
        } else {
            text = GameConstants.FIXED_NUMBER_OF_SHOTS;
        }

        return text;
    }

    public String getRows() {
        return rows.getText();
    }

    public String getColumns() {
        return columns.getText();
    }

    public Label getGameStartStateLabel() {
        return gameStartStateLabel;
    }
}
