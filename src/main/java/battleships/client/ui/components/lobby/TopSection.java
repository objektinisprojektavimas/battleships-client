package battleships.client.ui.components.lobby;

import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;

public class TopSection extends HBox {

    private Label label;

    public TopSection() {
        this.label = getLabel();
        initialize();
    }

    private void initialize() {
        getChildren().addAll(label);
        setAlignment(Pos.CENTER);
    }

    private Label getLabel() {
        Label label = new Label("Lobby Area");
        label.setFont(new Font("Arial", 30));;
        label.setMinSize(100, 50);
        return label;
    }
}
