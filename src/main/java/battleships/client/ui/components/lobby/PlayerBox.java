package battleships.client.ui.components.lobby;

import battleships.client.models.Client;
import battleships.client.ui.components.common.ComponentUtils;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

public class PlayerBox extends VBox {

    private Client client;
    private Label nameLabel;
    private Label ipLabel;

    public PlayerBox(Client client) {
        this.client = client;
        this.nameLabel = getNameLabel(client);
        this.ipLabel = getIPLabel(client);
        initialize();
    }

    private void initialize() {
        setStyle(cssStyle());
        getChildren().add(nameLabel);
        getChildren().add(ComponentUtils.getHorizontalSeparator());
        getChildren().add(ipLabel);
    }

    private String cssStyle() {
        return  "-fx-padding: 8px;\n" +
                "-fx-border-insets: 8px;" +
                "-fx-border-color: black;\n" +
                "-fx-border-insets: 5;\n" +
                "-fx-border-width: 1;\n";
    }

    private Label getNameLabel(Client client) {
        Label label = new Label(client.getName());
        label.setFont(new Font("Arial", 20));
        return label;
    }

    private Label getIPLabel(Client client) {
        Label label = new Label(client.getIp());
        label.setFont(new Font("Arial", 12));
        return label;
    }
}
