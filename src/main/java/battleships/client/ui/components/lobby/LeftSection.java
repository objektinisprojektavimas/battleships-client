package battleships.client.ui.components.lobby;

import battleships.client.models.Client;
import javafx.geometry.Insets;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import java.util.List;

public class LeftSection extends HBox {

    private VBox clientsSection;

    public LeftSection() {
        this.clientsSection = getClientsSection();
        initialize();
    }

    private void initialize() {
        getChildren().add(clientsSection);
    }

    private VBox getClientsSection() {
        VBox clientsBoxSection = new VBox();
        clientsBoxSection.setMinWidth(175);
        return clientsBoxSection;
    }

    public void updateClientsSection(List<Client> clients) {
        clientsSection.getChildren().clear();
        for (Client client : clients) {
            PlayerBox playerBox = new PlayerBox(client);
            clientsSection.getChildren().add(playerBox);
            VBox.setMargin(playerBox, new Insets(0, 17, 0, 0));
        }
    }
}
