package battleships.client.ui.components.lobby;

import battleships.client.models.GameSettings;
import battleships.client.services.ConnectionService;
import battleships.client.ui.components.common.ComponentUtils;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class CenterSection extends VBox {

    private Button start;
    private SettingsSection settingsSection;
    private ConnectionService connectionService;

    public CenterSection() {
        createStartButton();
        createSettingsSection();
        setEvents();
        initialize();
    }

    private void createStartButton() {
        this.start = new Button("Start");
        this.connectionService = new ConnectionService();
        start.setMinWidth(300);
    }

    private void createSettingsSection() {
        this.settingsSection = new SettingsSection();
    }

    private void initialize() {
        HBox startSection = new HBox();
        startSection.setAlignment(Pos.CENTER);
        startSection.getChildren().add(start);

        getChildren().add(ComponentUtils.getHorizontalSeparator());
        getChildren().add(settingsSection);
        getChildren().add(startSection);
    }

    private void setEvents() {
        start.setOnAction(event -> {
            if (settingsSection.isValid()) {
                settingsSection.toggleSettingsSection(true);
                GameSettings settings = settingsSection.getGameSettings();
                connectionService.start(settings);
                settingsSection.startSettingsUpdateLoop();
            }
        });
    }

    public SettingsSection getSettingsSection() {
        return settingsSection;
    }
}
