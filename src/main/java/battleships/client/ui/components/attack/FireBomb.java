package battleships.client.ui.components.attack;

public class FireBomb extends Bomb {
    private int row;
    private int column;

    public FireBomb(final int row, final int column, final AttackAPI attackAPI) {
        super(attackAPI);
        this.row = row;
        this.column = column;
    }

    public void hit(){
        attackAPI.hitTarget(row, column);
    }
}
