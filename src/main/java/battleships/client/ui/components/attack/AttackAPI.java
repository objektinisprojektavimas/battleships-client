package battleships.client.ui.components.attack;

interface AttackAPI {
    public void hitTarget(final int row, final int column);
}
