package battleships.client.ui.components.attack;

abstract class Bomb {
    protected AttackAPI attackAPI;

    protected Bomb(final AttackAPI attackAPI){
        this.attackAPI = attackAPI;
    }

    public abstract void hit();
}
