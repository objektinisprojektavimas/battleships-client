package battleships.client.ui.components.gameinitiation;

import battleships.client.enums.ShipType;
import battleships.client.factory.ShipFactory;
import battleships.client.ui.components.common.ComponentUtils;
import battleships.client.ui.components.common.GameMap;
import javafx.geometry.Insets;
import javafx.scene.layout.VBox;

public class ShipsSection extends VBox {

    private GameMap gameMap;
    private VBox carrierShipBox;
    private VBox destroyerShipBox;
    private VBox submarineShipBox;
    private VBox cruiserShipBox;
    private VBox battleshipShipBox;

    public ShipsSection(GameMap gameMap) {
        this.gameMap = gameMap;
        this.carrierShipBox = new ShipBox(ShipFactory.createShip(ShipType.CARRIER), gameMap);
        this.destroyerShipBox = new ShipBox(ShipFactory.createShip(ShipType.DESTROYER), gameMap);
        this.submarineShipBox = new ShipBox(ShipFactory.createShip(ShipType.SUBMARINE), gameMap);
        this.cruiserShipBox = new ShipBox(ShipFactory.createShip(ShipType.CRUISER), gameMap);
        this.battleshipShipBox = new ShipBox(ShipFactory.createShip(ShipType.BATTLESHIP), gameMap);
        cssStyle();
        initialize();
    }

    private void initialize() {
        getChildren().add(ComponentUtils.getEmptyRow());
        getChildren().add(carrierShipBox);
        getChildren().add(ComponentUtils.getEmptyRow());
        getChildren().add(destroyerShipBox);
        getChildren().add(ComponentUtils.getEmptyRow());
        getChildren().add(submarineShipBox);
        getChildren().add(ComponentUtils.getEmptyRow());
        getChildren().add(cruiserShipBox);
        getChildren().add(ComponentUtils.getEmptyRow());
        getChildren().add(battleshipShipBox);
    }

    private void cssStyle() {
        setPadding(new Insets(20, 15, 0, 15));
    }
}
