package battleships.client.ui.components.gameinitiation;

import battleships.client.constants.StyleConstants;
import battleships.client.factory.MapHoverStrategyFactory;
import battleships.client.models.ships.Ship;
import battleships.client.strategies.AbstractMapHoverStrategy;
import battleships.client.ui.components.common.ComponentUtils;
import battleships.client.ui.components.common.CssStyleHelper;
import battleships.client.ui.components.common.GameMap;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

public class ShipBox extends VBox {

    private GameMap gameMap;
    private Ship ship;
    private Label shipType;
    private Label shipCost;
    private ImageView shipImageView;
    private StackPane shipImageViewStackPane;
    private CssStyleHelper cssStyleHelper;

    private final Background focusBackground = new Background( new BackgroundFill( Color.BLUEVIOLET, CornerRadii.EMPTY, Insets.EMPTY ) );
    private final Background unfocusBackground = new Background( new BackgroundFill( Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY ) );

    public ShipBox(Ship ship, GameMap gameMap) {
        this.ship = ship;
        this.gameMap = gameMap;
        this.shipType = ComponentUtils.getLabel(ship.getType().getText(),
                new CssStyleHelper().fontSize(15));
        this.shipCost = ComponentUtils.getLabel("Cost: " + String.valueOf(ship.getCost()),
                new CssStyleHelper().fontSize(12));
        this.shipImageView = new ImageView(ship.getImage());
        this.shipImageViewStackPane = new StackPane(shipImageView);
        this.cssStyleHelper = new CssStyleHelper();
        setStyleProperties();
        updateStyle();
        setOnMouseEnteredEvent();
        setOnMouserExitedEvent();
        setOnClickEvent();
        initialize();
    }

    private void initialize() {
        getChildren().add(ComponentUtils.getEmptyRow());
        getChildren().add(shipImageViewStackPane);
        //getChildren().add(ComponentUtils.getHorizontalSeparator());
        getChildren().add(shipType);
        //getChildren().add(ComponentUtils.getEmptyRow());
        getChildren().add(shipCost);
    }

    private void setStyleProperties() {
        shipImageView.setFitWidth(125);
        shipImageView.setFitHeight(100);
        shipImageView.setPreserveRatio(true);
        cssStyleHelper.padding(15).borderWidth(1).borderColor("black").borderRadius(15).backgroundRadius(15);
    }

    private void updateStyle() {
        setStyle(cssStyleHelper.getStyle());
    }

    private void setOnMouseEnteredEvent() {
        setOnMouseEntered(event -> {
            cssStyleHelper.backgroundColor("#DDDDDD");
            updateStyle();
        });
    }

    private void setOnMouserExitedEvent() {
        setOnMouseExited(event -> {
            cssStyleHelper.backgroundColor(StyleConstants.DEFAULT_SHIP_BOX_BACKGROUND_COLOR);
            updateStyle();
        });
    }

    private void setOnClickEvent() {
        setOnMouseClicked(event -> {
            AbstractMapHoverStrategy hoverStrategy = MapHoverStrategyFactory.createMapHoverStrategy(ship.getType(), gameMap);
            gameMap.setHoverStrategy(hoverStrategy);

            cssStyleHelper.backgroundColor("#DDDDDD");
            updateStyle();
        });
    }
}
