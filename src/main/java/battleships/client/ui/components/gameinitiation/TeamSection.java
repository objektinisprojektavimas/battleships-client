package battleships.client.ui.components.gameinitiation;

import battleships.client.models.Team;
import battleships.client.ui.components.common.ComponentUtils;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class TeamSection extends VBox {

    private VBox teamMembers;
    private Label teamTitle;
    private Team team;

    public TeamSection(Team team) {
        this.teamMembers = createTeamMembers();
        this.teamTitle = ComponentUtils.getDefaultLabel(team.getTeamTitle());
        this.team = team;
        initialize();
    }

    private void initialize() {
        HBox teamTitleHBox = new HBox(teamTitle);
        teamTitleHBox.setAlignment(Pos.CENTER);
        getChildren().add(ComponentUtils.getEmptyRow());
        getChildren().add(teamTitleHBox);
        getChildren().add(ComponentUtils.getEmptyRow());
        getChildren().add(teamMembers);
    }

    private VBox createTeamMembers() {
        VBox teamMembersBoxesColumn = new VBox();
        teamMembersBoxesColumn.setMinWidth(175);
        return teamMembersBoxesColumn;
    }

    public void updateTeamSection() {
        teamMembers.getChildren().clear();
        team.getPlayers()
            .forEach(player -> teamMembers.getChildren().add(new TeamMemberBox(player)));
    }
}
