package battleships.client.ui.components.gameinitiation;

import battleships.client.ui.components.common.GameMap;
import javafx.scene.layout.VBox;

public class GameMapSection extends VBox {

    private GameMap gameMap;

    public GameMapSection(GameMap gameMap) {
        this.gameMap = gameMap;
        initialize();
    }

    private void initialize() {
        getChildren().add(gameMap);
    }
}
