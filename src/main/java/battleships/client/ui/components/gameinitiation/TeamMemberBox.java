package battleships.client.ui.components.gameinitiation;

import battleships.client.models.Player;
import battleships.client.ui.components.common.ComponentUtils;
import battleships.client.ui.components.common.CssStyleHelper;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;


public class TeamMemberBox extends VBox {

    private Player player;
    private Label name;
    private Label gold;
    private Label isPicking;
    private Button done;
    private CssStyleHelper cssStyleHelper;

    public TeamMemberBox(Player player) {
        this.player = player;
        this.name = ComponentUtils.getLabel(player.getClient().getName(),
                new CssStyleHelper().fontFamily("Arial").fontSize(18));
        this.gold = ComponentUtils.getLabel(String.valueOf("Gold: "+ player.getGold()),
                new CssStyleHelper().fontFamily("Arial").fontSize(12));
        this.isPicking = ComponentUtils.getLabel("is picking",
                new CssStyleHelper().fontFamily("Arial").fontSize(10).fontWeight("bold"));
        this.done = ComponentUtils.getButton("Done");
        this.cssStyleHelper = cssStyle();
        initialize();
    }

    private void initialize() {
        setStyle(cssStyleHelper.getStyle());
        getChildren().add(name);
        getChildren().add(ComponentUtils.getHorizontalSeparator());
        getChildren().add(gold);
    }

    private CssStyleHelper cssStyle() {
        return new CssStyleHelper().padding(8).borderInsets(8).borderColor("black").borderWidth(1);
    }

    public void enableIsPicking() {
        if (!getChildren().contains(isPicking)) {
            getChildren().add(isPicking);
        }
    }

    public void disableIsPicking() {
        if (getChildren().contains(isPicking)) {
            getChildren().remove(isPicking);
        }
    }

    public void setBorderColor(String color) {
        this.cssStyleHelper.borderColor(color);
        updateStyle();
    }

    public void setBorderWidth(int width) {
        this.cssStyleHelper.borderWidth(width);
        updateStyle();
    }

    public void updateStyle() {
        setStyle(cssStyleHelper.getStyle());
    }

    public void addDoneButton() {
        getChildren().add(done);
    }

    public void removeAddButton() {
        getChildren().remove(done);
    }

    public void disableDoneButton() {
        this.done.setDisable(true);
    }

    public void enableDoneButton() {
        this.done.setDisable(false);
    }

    public void setDoneAction(EventHandler<ActionEvent> event) {
        this.done.setOnAction(event);
    }
}
