package battleships.client.ui.components.menu;

import battleships.client.ui.components.common.ComponentUtils;
import battleships.client.constants.PropertiesConstants;
import battleships.client.ui.scenes.LobbyScene;
import battleships.client.services.ConnectionService;
import battleships.client.utils.PropertiesUtils;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class LeftSection extends HBox {

    private ConnectionService connectionService;
    private Stage primaryStage;
    private VBox contentSection;
    private Button multiplayerButton;
    private TextField playerNameTextField;
    private LobbyScene lobbyScene;

    public LeftSection(Stage primaryStage) {
        this.connectionService = new ConnectionService();
        this.primaryStage = primaryStage;
        this.playerNameTextField = createPlayerNameTextField();
        this.multiplayerButton = getMultiplayerButton();
        this.contentSection = getContentSection(playerNameTextField, multiplayerButton);
        initialize();
    }

    private void initialize() {
        getChildren().add(contentSection);
        getChildren().add(ComponentUtils.getVerticalSeparator());
    }

    private Button getMultiplayerButton() {
        Button button = new Button("Multiplayer");
        button.setMinWidth(PropertiesUtils.getPropertyAsInt(PropertiesConstants.BUTTON_MULTIPLAYER_WIDTH));
        button.setOnAction(event -> {
            String name = playerNameTextField.getText();
            if (!name.isEmpty() && connectionService.connectToLobby(name)) {
                primaryStage.setScene(new LobbyScene(new BorderPane(), primaryStage));
            }
        });
        return button;
    }

    private VBox getContentSection(TextField playerNameTextField, Button multiplayerButton) {
        VBox contentSection = new VBox();
        contentSection.setMinWidth(PropertiesUtils.getPropertyAsInt(PropertiesConstants.MENU_LEFT_PANEL_WIDTH));
        contentSection.setSpacing(PropertiesUtils.getPropertyAsInt(PropertiesConstants.MENU_LEFT_PANEL_SPACING));
        contentSection.getChildren().add(playerNameTextField);
        contentSection.getChildren().add(multiplayerButton);
        contentSection.setMargin(playerNameTextField, new Insets(20, 5, 0, 5));
        contentSection.setAlignment(Pos.TOP_CENTER);
        return contentSection;
    }

    private TextField createPlayerNameTextField() {
        TextField playerNameTextField = new TextField();
        playerNameTextField.setPromptText("Client name");
        return playerNameTextField;
    }

    private LobbyScene getLobbyScene(BorderPane root, Stage primaryStage) {
        LobbyScene lobbyScene = new LobbyScene(root, primaryStage);
        return lobbyScene;
    }
}
