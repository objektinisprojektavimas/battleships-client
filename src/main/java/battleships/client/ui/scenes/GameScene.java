package battleships.client.ui.scenes;

import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class GameScene extends Scene {

    public GameScene(BorderPane root, Stage primaryStage) {
        super(root);
        root.getChildren().add(new Label("Game has started..."));
    }
}
