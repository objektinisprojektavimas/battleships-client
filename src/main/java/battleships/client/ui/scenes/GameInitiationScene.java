package battleships.client.ui.scenes;

import battleships.client.models.Client;
import battleships.client.models.Player;
import battleships.client.models.Team;
import battleships.client.ui.components.common.GameMap;
import battleships.client.ui.components.gameinitiation.GameMapSection;
import battleships.client.ui.components.gameinitiation.ShipsSection;
import battleships.client.ui.components.gameinitiation.TeamSection;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.List;

public class GameInitiationScene extends Scene {

    private Team team;
    private GameMap gameMap;

    private BorderPane root;
    private TeamSection teamSection;
    private GameMapSection gameMapSection;
    private ShipsSection shipsSection;

    public GameInitiationScene(BorderPane root, Stage primaryStage) {
        super(root);
        this.root = root;
        this.team = getTeam();
        this.gameMap = getGameMap();
        createTeamSection();
        createGameMapSection();
        createShipsSection();
        initialize();
    }

    private void initialize() {
        root.setLeft(teamSection);
        root.setCenter(gameMapSection);
        root.setRight(shipsSection);
    }

    private void createGameMapSection() {
        this.gameMapSection = new GameMapSection(gameMap);
    }

    private void createTeamSection() {
        this.teamSection = new TeamSection(team);
        teamSection.updateTeamSection();
    }

    private void createShipsSection() {
        this.shipsSection = new ShipsSection(gameMap);
    }

    private GameMap getGameMap() {
        GameMap gameMap = new GameMap(10, 10);
        return gameMap;
    }

    private Team getTeam() {
        List<Player> players = new ArrayList<>();
        Client client1 = new Client();
        client1.setIp("123.456.789");
        client1.setId("someClientId1");
        client1.setName("Client 1");
        Player player1 = new Player(client1, 100);

        Client client2 = new Client();
        client2.setIp("123.456.789");
        client2.setId("someClientId2");
        client2.setName("Client 2");
        Player player2 = new Player(client2, 150);
        players.add(player1);
        players.add(player2);

        Team team = new Team(players, "Team Title1");
        return team;
    }
}
