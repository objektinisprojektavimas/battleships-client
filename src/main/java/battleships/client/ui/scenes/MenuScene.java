package battleships.client.ui.scenes;

import battleships.client.ui.components.menu.LeftSection;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class MenuScene extends Scene {

    private Stage primaryStage;

    private BorderPane root;
    private LeftSection leftSection;

    public MenuScene(BorderPane root, Stage primaryStage) {
        super(root);
        this.root = root;
        this.primaryStage = primaryStage;
        this.leftSection = new LeftSection(primaryStage);
        initialize();
    }

    private void initialize() {
        root.setLeft(leftSection);
    }
}
