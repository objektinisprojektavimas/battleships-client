package battleships.client.ui.scenes;

import battleships.client.constants.GameConstants;
import battleships.client.models.Client;
import battleships.client.models.GameStartState;
import battleships.client.services.ConnectionService;
import battleships.client.ui.components.lobby.CenterSection;
import battleships.client.ui.components.lobby.LeftSection;
import battleships.client.ui.components.lobby.TopSection;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.Scene;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.util.List;

public class LobbyScene extends Scene {

    private ConnectionService connectionService;

    private Stage primaryStage;
    private BorderPane root;
    private TopSection topSection;
    private LeftSection leftSection;
    private CenterSection centerSection;
    private Timeline timeline;

    public LobbyScene(BorderPane root, Stage primaryStage) {
        super(root);
        this.root = root;
        this.primaryStage = primaryStage;
        this.topSection = new TopSection();
        this.leftSection = new LeftSection();
        this.centerSection = new CenterSection();
        this.connectionService = new ConnectionService();
        initialize();
        startLoop();
    }

    private void initialize() {
        ScrollPane scroll = new ScrollPane();
        scroll.setContent(leftSection);

        root.setTop(topSection);
        root.setCenter(centerSection);
        root.setLeft(scroll);
    }

    public void startLoop() {
        this.timeline = new Timeline(
                new KeyFrame(Duration.millis(1000), e -> {
                    connectionService.ping();
                    updateClients();
                    handleGameState();
                })
        );
        timeline.setCycleCount(Animation.INDEFINITE);
        timeline.play();
    }

    public void stopLoop() {
        this.timeline.stop();
    }

    private void updateClients() {
        List<Client> clients = connectionService.getAllClients();
        leftSection.updateClientsSection(clients);
    }

    private void handleGameState() {
        GameStartState gameStartState = connectionService.getGameStartState();
        if (gameStartState.getState().equals(GameConstants.NOT_STARTED)) {
            centerSection.getSettingsSection()
                         .getGameStartStateLabel()
                         .setText(gameStartState.getAgreedClients() + "/" + gameStartState.getAllClients() + " clients agreed to start");
        } else if (gameStartState.getState().equals(GameConstants.STARTED)){
            stopLoop();
            primaryStage.setScene(new GameInitiationScene(new BorderPane(), primaryStage));
        }
    }
}
