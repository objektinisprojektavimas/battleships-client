package battleships.client.commands;

import battleships.client.enums.ShipType;
import battleships.client.ui.components.common.GameMap;

public abstract class AbstractPlaceShipCommand {

    private GameMap gameMap;

    public AbstractPlaceShipCommand(GameMap gameMap) {
        this.gameMap = gameMap;
    }

    public abstract void placeShip(int row, int column, ShipType ship);
}
