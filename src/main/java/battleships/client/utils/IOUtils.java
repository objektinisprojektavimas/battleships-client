package battleships.client.utils;

import java.io.InputStream;

public class IOUtils {

    public static InputStream getResourceAsStream(String name) {
        return IOUtils.class.getClassLoader().getResourceAsStream(name);
    }
}
