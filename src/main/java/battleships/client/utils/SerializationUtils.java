package battleships.client.utils;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

public class SerializationUtils {

    private static Gson GSON = new Gson();

    /**
     * Serialize object to Json String.
     * @param object to be serialized.
     * @return Json String
     */
    public static String toJson(Object object) {
        return GSON.toJson(object);
    }

    /**
     * Deserializes json into an object of the specified class.
     * @param json the string from which the object is to be deserialized.
     * @return deserialized object
     */
    public static <T> T fromJson(String json, Class<T> clazz) {
        try {
            return GSON.fromJson(json, clazz);
        } catch (JsonSyntaxException ex) {
            System.out.println("Unable to deserialize json: " + json + ", into a class of type " + clazz);
        }
        return null;
    }
}
