package battleships.client.utils;

import battleships.client.constants.PropertiesConstants;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesUtils {

    private static Properties properties = initialize();

    public static Properties initialize() {
        Properties properties = new Properties();
        InputStream input = IOUtils.getResourceAsStream(PropertiesConstants.PROPERTIES_FILE);

        try {
            properties.load(input);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return properties;
    }

    public static String getProperty(String key) {
        return properties.getProperty(key);
    }

    public static int getPropertyAsInt(String key) {
        return Integer.valueOf(properties.getProperty(key));
    }

    public static double getPropertyAsDouble(String key) {
        return Double.valueOf(properties.getProperty(key));
    }

    public static boolean getPropertyAsBoolean(String key) {
        return Boolean.valueOf(properties.getProperty(key));
    }

}
