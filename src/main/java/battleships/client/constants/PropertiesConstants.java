package battleships.client.constants;

public class PropertiesConstants {

    public static final String PROPERTIES_FILE = "application.properties";

    public static final String GAME_NAME = "game.name";
    public static final String WEBAPP_HOST = "webapp.host";
    public static final String STAGE_WIDTH = "stage.width";
    public static final String STAGE_HEIGHT = "stage.height";
    public static final String MENU_LEFT_PANEL_WIDTH = "left.panel.width";
    public static final String MENU_LEFT_PANEL_SPACING = "left.panel.spacing";
    public static final String BUTTON_MULTIPLAYER_WIDTH = "button.multiplayer.width";

}
