package battleships.client.constants;

import battleships.client.enums.Orientation;

public class GameConstants {

    public static final String STARTED = "STARTED";
    public static final String NOT_STARTED = "NOT_STARTED";

    public static final String NUMBER_OF_SHOTS_BY_UNSUNKEN_SHIPS = "NUMBER_OF_SHOTS_BY_UNSUNKEN_SHIPS";
    public static final String NUMBER_OF_SHOTS_BY_LARGEST_SHIP = "NUMBER_OF_SHOTS_BY_LARGEST_SHIP";
    public static final String FIXED_NUMBER_OF_SHOTS = "FIXED_NUMBER_OF_SHOTS";

    public static final Orientation DEFAULT_SHIP_ORIENTATION = Orientation.HORIZONTAL;

    public static final String BATTLESHIP_IMAGE_PATH = "images/Battleship.png";
    public static final String CARRIER_IMAGE_PATH = "images/Carrier.png";
    public static final String CRUISER_IMAGE_PATH = "images/Cruiser.png";
    public static final String DESTROYER_IMAGE_PATH = "images/Destroyer.png";
    public static final String SUBMARINE_IMAGE_PATH = "images/Submarine.png";

}
