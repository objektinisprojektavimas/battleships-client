package battleships.client.constants;

public class HeaderConstants {

    public static final String CONTENT_TYPE = "Content-Type";
    public static final String ACCEPT =  "Accept";

    public static final String APPLICATION_JSON = "application/json";
}
