package battleships.client.constants;

public class StyleConstants {

    public static final String DEFAULT_MAP_CELL_COLOR = "white";
    public static final String DEFAULT_SHIP_BOX_BACKGROUND_COLOR = "white";
}
