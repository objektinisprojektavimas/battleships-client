package battleships.client;

import battleships.client.constants.PropertiesConstants;
import battleships.client.ui.scenes.MenuScene;
import battleships.client.utils.PropertiesUtils;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class BattleshipsApplication extends Application {

    private Stage primaryStage;
    private Scene menuScene;

    @Override
    public void start(Stage primaryStage) throws Exception {
        this.primaryStage = primaryStage;
        this.menuScene = createMenuScene(primaryStage);
        initialize();
    }

    private void initialize() {
        primaryStage.setTitle(PropertiesUtils.getProperty(PropertiesConstants.GAME_NAME));
        primaryStage.setWidth(PropertiesUtils.getPropertyAsInt(PropertiesConstants.STAGE_WIDTH));
        primaryStage.setHeight(PropertiesUtils.getPropertyAsInt(PropertiesConstants.STAGE_HEIGHT));
        primaryStage.setScene(menuScene);
        primaryStage.show();
    }

    private Scene createMenuScene(Stage primaryStage) {
        MenuScene menuScene = new MenuScene(new BorderPane(), primaryStage);
        return menuScene;
    }

    public static void main(String[] args) {
        launch(args);
    }

}
