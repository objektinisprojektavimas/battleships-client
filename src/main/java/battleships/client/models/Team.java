package battleships.client.models;

import java.util.List;

public class Team {

    private List<Player> players;
    private String teamTitle;

    public Team(List<Player> players, String teamTitle) {
        this.players = players;
        this.teamTitle = teamTitle;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }

    public String getTeamTitle() {
        return teamTitle;
    }

    public void setTeamTitle(String teamTitle) {
        this.teamTitle = teamTitle;
    }
}
