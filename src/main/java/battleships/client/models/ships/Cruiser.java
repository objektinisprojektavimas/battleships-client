package battleships.client.models.ships;

import battleships.client.constants.GameConstants;
import battleships.client.enums.ShipType;
import battleships.client.utils.IOUtils;
import javafx.scene.image.Image;

public class Cruiser extends Ship {

    public Cruiser() {
        setLenght(2);
        setCost(35);
        setType(ShipType.CRUISER);
        setImage(new Image(IOUtils.getResourceAsStream(GameConstants.CRUISER_IMAGE_PATH)));
    }
}
