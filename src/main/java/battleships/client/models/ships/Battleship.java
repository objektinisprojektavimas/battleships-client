package battleships.client.models.ships;

import battleships.client.constants.GameConstants;
import battleships.client.enums.ShipType;
import battleships.client.utils.IOUtils;
import javafx.scene.image.Image;

public class Battleship extends Ship {

    public Battleship() {
        setLenght(5);
        setCost(90);
        setType(ShipType.BATTLESHIP);
        setImage(new Image(IOUtils.getResourceAsStream(GameConstants.BATTLESHIP_IMAGE_PATH)));
    }
}
