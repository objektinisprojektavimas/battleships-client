package battleships.client.models.ships;

import battleships.client.constants.GameConstants;
import battleships.client.enums.ShipType;
import battleships.client.utils.IOUtils;
import javafx.scene.image.Image;

public class Destroyer extends Ship {

    public Destroyer() {
        setLenght(4);
        setCost(80);
        setType(ShipType.DESTROYER);
        setImage(new Image(IOUtils.getResourceAsStream(GameConstants.DESTROYER_IMAGE_PATH)));
    }
}
