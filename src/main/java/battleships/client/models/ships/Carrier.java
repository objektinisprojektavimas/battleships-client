package battleships.client.models.ships;

import battleships.client.constants.GameConstants;
import battleships.client.enums.ShipType;
import battleships.client.utils.IOUtils;
import javafx.scene.image.Image;

public class Carrier extends Ship {

    public Carrier() {
        setLenght(4);
        setCost(75);
        setType(ShipType.CARRIER);
        setImage(new Image(IOUtils.getResourceAsStream(GameConstants.CARRIER_IMAGE_PATH)));
    }
}
