package battleships.client.models.ships;

import battleships.client.builder.Prototype;
import battleships.client.constants.GameConstants;
import battleships.client.enums.ShipType;
import battleships.client.enums.Orientation;
import javafx.scene.image.Image;

public class Ship {

    private int hitpoints;
    private Image image;
    private ShipType type;
    private Orientation orientation;
    private int row;
    private int column;
    private int lenght;
    private int cost;
    private Prototype cl = null;

    public Ship() {
        orientation = GameConstants.DEFAULT_SHIP_ORIENTATION;
    }

    public void setHitpoints(int hitpoints) {
        this.hitpoints = hitpoints;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public void setType(ShipType type) {
        this.type = type;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public void setColumn(int column) {
        this.column = column;
    }

    public void setLenght(int lenght) {
        this.lenght = lenght;
    }

    public int getHitpoints() {
        return hitpoints;
    }

    public Image getImage() {
        return image;
    }

    public ShipType getType() {
        return type;
    }

    public int getRow() {
        return row;
    }

    public int getColumn() {
        return column;
    }

    public int getLenght() {
        return lenght;
    }

    public Orientation getOrientation() {
        return orientation;
    }

    public void setOrientation(Orientation orientation) {
        this.orientation = orientation;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public Ship shallowCopy() {
        try {
            return (Ship)super.clone();
        } catch (CloneNotSupportedException var2) {
            var2.printStackTrace();
            return null;
        }
    }

    public Ship deepCopy() {
        try {
            Ship qwe = (Ship)super.clone();
            qwe.cl = qwe.cl.clone();
            return qwe;
        } catch (CloneNotSupportedException var2) {
            var2.printStackTrace();
            return null;
        }
    }
}
