package battleships.client.models.ships;

import battleships.client.constants.GameConstants;
import battleships.client.enums.ShipType;
import battleships.client.utils.IOUtils;
import javafx.scene.image.Image;

public class Submarine extends Ship {

    public Submarine() {
        setLenght(2);
        setCost(65);
        setType(ShipType.SUBMARINE);
        setImage(new Image(IOUtils.getResourceAsStream(GameConstants.SUBMARINE_IMAGE_PATH)));
    }
}
