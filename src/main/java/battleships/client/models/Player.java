package battleships.client.models;

public class Player {

    private Client client;
    private int gold;

    public Player(Client client, int gold) {
        this.client = client;
        this.gold = gold;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public int getGold() {
        return gold;
    }

    public void setGold(int gold) {
        this.gold = gold;
    }
}
