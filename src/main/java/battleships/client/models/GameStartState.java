package battleships.client.models;

public class GameStartState {

    private String state;
    private int allClients;
    private int agreedClients;

    public GameStartState(String state, int allClients, int agreedClients) {
        this.state = state;
        this.allClients = allClients;
        this.agreedClients = agreedClients;
    }

    public GameStartState() {}

    public String getState() {
        return state;
    }

    public int getAllClients() {
        return allClients;
    }

    public int getAgreedClients() {
        return agreedClients;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void setAllClients(int allClients) {
        this.allClients = allClients;
    }

    public void setAgreedClients(int agreedClients) {
        this.agreedClients = agreedClients;
    }
}
