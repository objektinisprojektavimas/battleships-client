package battleships.client.models;

import battleships.client.ui.components.lobby.SettingsSection;

public class GameSettings {

    private String rows;
    private String columns;
    private String numberOfShotsPerTurn;

    public GameSettings(SettingsSection settings) {
        this.rows = settings.getRows();
        this.columns = settings.getColumns();
        this.numberOfShotsPerTurn = settings.getRBSelectedText();
    }

    public GameSettings() {}

    public static boolean isValid(SettingsSection settings) {
        try {
            Integer.parseInt(settings.getRows());
            Integer.parseInt(settings.getColumns());
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public String getRows() {
        return rows;
    }

    public String getColumns() {
        return columns;
    }

    public String getNumberOfShotsPerTurn() {
        return numberOfShotsPerTurn;
    }
}
