package battleships.client.services;

import battleships.client.constants.HeaderConstants;
import battleships.client.models.Client;
import battleships.client.models.GameSettings;
import battleships.client.models.GameStartState;
import battleships.client.utils.SerializationUtils;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.apache.http.HttpStatus;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ConnectionService extends BaseService {

    private static Client client;

    public boolean connectToLobby(String name) {
        HttpResponse<JsonNode> response = null;
        try {
            response = Unirest.post(host + "/connect/lobby")
                    .header(HeaderConstants.CONTENT_TYPE, HeaderConstants.APPLICATION_JSON)
                    .body(name)
                    .asJson();
        } catch (UnirestException e) {
            e.printStackTrace();
        }

        if (response != null && response.getBody() != null && response.getStatus() == HttpStatus.SC_OK) {
            client = SerializationUtils.fromJson(response.getBody().toString(), Client.class);
            return true;
        }

        return false;
    }

    public List<Client> getAllClients() {
        HttpResponse<Client[]> response = null;
        try {
            response = Unirest.get(host + "/connect/clients")
                    .header(HeaderConstants.ACCEPT, HeaderConstants.APPLICATION_JSON)
                    .asObject(Client[].class);
        } catch (UnirestException e) {
            e.printStackTrace();
        }

        if (response != null && response.getBody() != null) {
            return Arrays.asList(response.getBody());
        }

        return Collections.EMPTY_LIST;
    }

    public void ping() {
        try {
            Unirest.post(host + "/connect/ping")
                    .header(HeaderConstants.CONTENT_TYPE, HeaderConstants.APPLICATION_JSON)
                    .body(client)
                    .asJson();
        } catch (UnirestException e) {
            e.printStackTrace();
        }
    }

    public void start(GameSettings gameSettings) {
        try {
            Unirest.post(host + "/game/start")
                    .header(HeaderConstants.CONTENT_TYPE, HeaderConstants.APPLICATION_JSON)
                    .queryString("id", client.getId())
                    .body(gameSettings)
                    .asJson();
        } catch (UnirestException e) {
            e.printStackTrace();
        }
    }

    public GameSettings getGameSettings() {
        HttpResponse<GameSettings> response = null;
        try {
            response = Unirest.get(host + "/game/settings")
                    .header(HeaderConstants.ACCEPT, HeaderConstants.APPLICATION_JSON)
                    .asObject(GameSettings.class);
        } catch (UnirestException e) {
            e.printStackTrace();
        }

        return response.getBody();
    }

    public GameStartState getGameStartState() {
        HttpResponse<GameStartState> response = null;
        try {
            response = Unirest.get(host + "/game/start/state")
                    .header(HeaderConstants.ACCEPT, HeaderConstants.APPLICATION_JSON)
                    .asObject(GameStartState.class);
        } catch (UnirestException e) {
            e.printStackTrace();
        }

        return response.getBody();
    }

    public static Client getClient() {
        return client;
    }
}
